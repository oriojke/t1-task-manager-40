package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {
    public DataYamlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
