package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBackupLoadRequest extends AbstractUserRequest {
    public DataBackupLoadRequest(@Nullable String token) {
        super(token);
    }
}
