package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlLoadJaxBRequest extends AbstractUserRequest {
    public DataXmlLoadJaxBRequest(@Nullable String token) {
        super(token);
    }
}
