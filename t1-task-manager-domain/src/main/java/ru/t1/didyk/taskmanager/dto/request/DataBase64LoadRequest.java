package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataBase64LoadRequest extends AbstractUserRequest {
    public DataBase64LoadRequest(@Nullable String token) {
        super(token);
    }
}
