package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExists(@Nullable String login);

    @NotNull
    Boolean isEmailExists(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @NotNull
    User removeOne(@Nullable User model);


    void clear();


    @NotNull List<User> findAll();


    @NotNull List<User> findAll(@Nullable Comparator<User> comparator);


    @Nullable User add(@Nullable User model);


    boolean existsById(@NotNull String id);


    @Nullable User findOneById(@NotNull String id);


    @Nullable User findOneByIndex(@Nullable Integer index);


    int getSize();


    @Nullable User remove(@Nullable User model);


    @Nullable User removeById(@NotNull String id);


    @Nullable User removeByIndex(@NotNull Integer index);


    void removeAll(@Nullable List<User> modelsRemove);


    @NotNull Collection<User> add(@NotNull Collection<User> models);


    @NotNull Collection<User> set(@NotNull Collection<User> models);


    void update(final @NotNull User object);


    @Nullable List<User> findAll(@Nullable Sort sort);
}
