package ru.t1.didyk.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Select("select * from sessions where user_id=#{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAllWithUserId(@Nullable @Param(value = "userId") String userId);

    @Select("select * from sessions")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAll();

    @Insert("insert into sessions (id , user_id, created, role)" +
            "values (#{id}, #{userId}, #{date}, #{role})")
    void add(@NotNull Session model);

    @Select("select * from sessions where id = #{id} and user_id=#{userId} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIdWithUserId(@Nullable @Param(value = "userId") String userId, @Nullable @Param(value = "id") String id);

    @Select("select * from sessions where id = #{id} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(@Nullable @Param(value = "id") String id);

    @Select("select count(*) from sessions where user_id=#{userId}")
    int getSize(@Nullable @Param(value = "userId") String userId);

    @Delete("delete from sessions where id = #{id}")
    void remove(@Nullable Session model);

    @Update("update sessions set user_id=#{userId}, created=#{date}, role=#{role}" +
            "where id=#{id}")
    void update(final @NotNull Session object);
}
