package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void clear(@Nullable String userId);

    @Nullable List<Project> findAll(@Nullable String userId);

    @Nullable List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @Nullable Project add(@Nullable String userId, @NotNull Project model);

    @Nullable Project add(@NotNull Project model);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Project findOneById(@Nullable String id);

    @Nullable Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable Project remove(@Nullable String userId, @Nullable Project model);

    @Nullable Project remove(@Nullable Project model);

    @Nullable Project removeById(@Nullable String userId, @Nullable String id);

    @Nullable Project removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull Collection<Project> set(@NotNull Collection<Project> models);

    @NotNull void update(final @NotNull Project object);

    @Nullable List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable List<Project> findAll();
}
