package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.Session;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ISessionService {


    void clear(@Nullable String userId);


    @Nullable List<Session> findAll(@Nullable String userId);


    @Nullable List<Session> findAll(@Nullable String userId, @Nullable Comparator<Session> comparator);


    @Nullable Session add(@Nullable String userId, @NotNull Session model);


    @Nullable Session add(@NotNull Session model);


    boolean existsById(@Nullable String userId, @Nullable String id);


    boolean existsById(@Nullable String id);


    @Nullable Session findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Session findOneById(@Nullable String id);


    @Nullable Session findOneByIndex(@Nullable String userId, @Nullable Integer index);


    int getSize(@Nullable String userId);


    @Nullable Session remove(@Nullable String userId, @Nullable Session model);


    @Nullable Session remove(@Nullable Session model);


    @Nullable Session removeById(@Nullable String userId, @Nullable String id);


    @Nullable Session removeByIndex(@Nullable String userId, @Nullable Integer index);


    void removeAll(@Nullable String userId);


    @NotNull Collection<Session> add(@NotNull Collection<Session> models);


    @NotNull Collection<Session> set(@NotNull Collection<Session> models);


    @NotNull void update(final @NotNull Session object);


    @Nullable List<Session> findAll(@Nullable String userId, @Nullable Sort sort);


    @Nullable List<Session> findAll();
}
