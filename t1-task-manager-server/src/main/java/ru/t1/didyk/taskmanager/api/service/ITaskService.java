package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);


    void clear(@Nullable String userId);


    @Nullable List<Task> findAll(@Nullable String userId);


    @Nullable List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);


    @Nullable Task add(@Nullable String userId, @NotNull Task model);


    @Nullable Task add(@NotNull Task model);


    boolean existsById(@Nullable String userId, @Nullable String id);


    boolean existsById(@Nullable String id);


    @Nullable Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Task findOneById(@Nullable String id);


    @Nullable Task findOneByIndex(@Nullable String userId, @Nullable Integer index);


    int getSize(@Nullable String userId);


    @Nullable Task remove(@Nullable String userId, @Nullable Task model);


    @Nullable Task remove(@Nullable Task model);


    @Nullable Task removeById(@Nullable String userId, @Nullable String id);


    @Nullable Task removeByIndex(@Nullable String userId, @Nullable Integer index);


    void removeAll(@Nullable String userId);


    @NotNull Collection<Task> add(@NotNull Collection<Task> models);


    @NotNull Collection<Task> set(@NotNull Collection<Task> models);


    @NotNull void update(final @NotNull Task object);


    @Nullable List<Task> findAll(@Nullable String userId, @Nullable Sort sort);


    @Nullable List<Task> findAll();
}
