package ru.t1.didyk.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.api.service.IUserService;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.didyk.taskmanager.exception.field.*;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.util.HashUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IConnectionService connectionService;

    public UserService(@NotNull IPropertyService propertyService, @NotNull IConnectionService connectionService) {
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @Override
    public void update(@NotNull User object) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(object);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            repository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = create(login, password);
            user.setEmail(email);
            repository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = create(login, password);
            user.setRole(role);
            repository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = repository.findByLogin(login);
            return user;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = repository.findByEmail(email);
            return user;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return removeOne(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    public void removeAll(@Nullable final List<User> modelsRemove) {
        if (modelsRemove == null) return;
        modelsRemove.forEach(model -> remove(model));
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = findByLogin(login);
            user.setLocked(true);
            repository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = findByLogin(login);
            user.setLocked(false);
            repository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeOne(@Nullable final User model) {
        if (model == null) return null;
        return remove(model);
    }


    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull List<User> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final List<User> result;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            result = repository.findAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @NotNull List<User> findAll(@Nullable Comparator<User> comparator) {
        @NotNull final List<User> result = findAll();
        result.sort(comparator);
        return result;
    }

    @Override
    public @Nullable User add(@Nullable User model) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable User findOneById(@NotNull String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final User result;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            result = repository.findOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable User findOneByIndex(@Nullable Integer index) {
        return findAll().get(index);
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        int result = -1;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            result = repository.getSize();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable User remove(@Nullable User model) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public @Nullable User removeById(@NotNull String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final User result;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            result = findById(id);
            repository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable User removeByIndex(@NotNull Integer index) {
        @Nullable final User model = findOneByIndex(index);
        if (model == null) return null;
        return removeById(model.getId());
    }

    @Override
    public @NotNull Collection<User> add(@NotNull Collection<User> models) {
        models.forEach(model -> add(model));
        return models;
    }

    @Override
    public @NotNull Collection<User> set(@NotNull Collection<User> models) {
        clear();
        return add(models);
    }

    @Override
    public @Nullable List<User> findAll(@Nullable Sort sort) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable List<User> result;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            result = repository.findAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }
}