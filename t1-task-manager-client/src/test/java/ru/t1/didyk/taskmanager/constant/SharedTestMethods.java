package ru.t1.didyk.taskmanager.constant;

import lombok.Getter;
import org.apache.commons.lang.RandomStringUtils;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.service.PropertyService;

public class SharedTestMethods {

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    public static String loginUser(@NotNull String login, @NotNull String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        final String userToken = authEndpoint.login(request).getToken();
        return userToken;
    }

    public static String getRandomString(int length) {
        String generatedString = RandomStringUtils.randomAlphanumeric(length);
        return generatedString;
    }

}
