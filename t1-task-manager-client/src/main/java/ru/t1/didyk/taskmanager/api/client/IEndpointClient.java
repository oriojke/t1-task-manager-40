package ru.t1.didyk.taskmanager.api.client;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    Socket connect() throws IOException;

    void disconnect() throws IOException;
}
